package ua.nure.cleaningservice.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.EditText;

import java.util.regex.Pattern;

import ua.nure.cleaningservice.R;


public class Verification {

    public static boolean verifyName(Context context, EditText input) {
        return true;
    }

    public static boolean verifyEmail(Context context, EditText input) {
        String email = input.getText().toString();
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        if (pattern.matcher(email).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.email_tip));
            return false;
        }
    }

    public static boolean verifyPassword(Context context, EditText input) {
        String password = input.getText().toString();
        Pattern PASSWORD_PATTERN = Pattern.compile("[a-zA-Z0-9_-]{8,24}");
        if (!TextUtils.isEmpty(password) && PASSWORD_PATTERN.matcher(password).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.password_tip));
            return false;
        }
    }

    public static boolean verifyPhone(Context context, EditText input) {
        String phone = input.getText().toString();
        Pattern PHONE_PATTER = Pattern.compile("^[+]*[(]?[0-9]{1,4}[)]?[-s./0-9]*$");
        if (!TextUtils.isEmpty(phone) && PHONE_PATTER.matcher(phone).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.phone_tip));
            return false;
        }
    }

    public static boolean verifyType(Context context, EditText input) {
        String type = input.getText().toString();
        Pattern TYPE_PATTER = Pattern.compile("^[A-Za-zА-Яа-я]+$");
        if (!TextUtils.isEmpty(type) && TYPE_PATTER.matcher(type).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.type_tip));
            return false;
        }
    }

    public static boolean verifyFloor(Context context, EditText input) {
        String floor = input.getText().toString();
        Pattern FLOOR_PATTER = Pattern.compile("^[0-9]+$");
        if (!TextUtils.isEmpty(floor) && FLOOR_PATTER.matcher(floor).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.floor_tip));
            return false;
        }
    }


    public static boolean verifyWinCount(Context context, EditText input) {
        String winCount = input.getText().toString();
        Pattern WIN_COUNT_PATTER = Pattern.compile("^[0-9]+$");
        if (!TextUtils.isEmpty(winCount) && WIN_COUNT_PATTER.matcher(winCount).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.win_count_tip));
            return false;
        }
    }

    public static boolean verifyArea(Context context, EditText input) {
        String area = input.getText().toString();
        Pattern AREA_PATTER = Pattern.compile("^[0-9]+$");
        if (!TextUtils.isEmpty(area) && AREA_PATTER.matcher(area).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.area_tip));
            return false;
        }
    }

    public static boolean verifyPrice(Context context, EditText input) {
        String price = input.getText().toString();
        Pattern PRICE_PATTER = Pattern.compile("^[0-9]+$");
        if (!TextUtils.isEmpty(price) && PRICE_PATTER.matcher(price).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.price_tip));
            return false;
        }
    }

    public static boolean verifyDesc(Context context, EditText input) {
        String desc = input.getText().toString();
        if (!TextUtils.isEmpty(desc)) {
            return true;
        } else {
            input.setError(context.getString(R.string.desc_tip));
            return false;
        }
    }

}
