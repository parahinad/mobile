package ua.nure.cleaningservice.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

public class Location {

    private static final String TAG = ".util.Location";

    private static android.location.Location lastLocation;

    public static android.location.Location getLastLocation() {
        return lastLocation;
    }

    public static void renewLocation(Activity activity, int requestCode, Runnable runnable) {
        if(ActivityCompat.checkSelfPermission(
                activity, Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[] {
                    Manifest.permission.ACCESS_FINE_LOCATION }, requestCode);
            return;
        }

        LocationManager locationManager = (LocationManager) activity.getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000L, 1.0f, new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull android.location.Location location) {
                Log.e(TAG, String.valueOf(location));
                if(location != null) {
                    lastLocation = location;
                    if (runnable != null) {
                        activity.runOnUiThread(runnable);
                    }
                }
            }
        });
    }

    public static void getAddressByLocation(Activity activity, int requestCode, Runnable runable){
        if (lastLocation == null) {
            renewLocation(activity, requestCode, () -> getAddressByLocation(activity, requestCode, runable));
        } else {
            try {
                Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
                List<Address> addresses = null;
                addresses = geocoder.getFromLocation(lastLocation.getLatitude(), lastLocation.getLongitude(), 1);
                String city = addresses.get(0).getLocality();
                String country = addresses.get(0).getCountryName();
                String addressLine = addresses.get(0).getAddressLine(0);
                String street = "", house = "";

                if(addressLine != null) {
                    String[] arr = addressLine.split(",");
                    street = arr[0];
                    house = arr[1];
                }

                ua.nure.cleaningservice.data.Address.getInstance()
                        .setCountry(country)
                        .setCity(city)
                        .setStreet(street)
                        .setHouseNumber(house)
                        .setLongitude(String.format(Locale.getDefault(), "%f",lastLocation.getLongitude()))
                        .setLatitude(String.format(Locale.getDefault(), "%f",lastLocation.getLatitude()));

                activity.runOnUiThread(runable);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}