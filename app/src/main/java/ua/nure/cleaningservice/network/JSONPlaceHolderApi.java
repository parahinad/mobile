package ua.nure.cleaningservice.network;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import ua.nure.cleaningservice.data.Company;
import ua.nure.cleaningservice.data.Contract;
import ua.nure.cleaningservice.data.Room;
import ua.nure.cleaningservice.data.Service;

public interface JSONPlaceHolderApi {

    @POST("/auth/login")
    Call<Company> login(@Body Company company);

    @GET("/cleaning-companies/{email}")
    Call<Company> getCleaningCompanyData(@Header("Authorization") String token, @Path("email") String email);

    @GET("/customer-companies/{email}")
    Call<Company> getCustomerCompanyData(@Header("Authorization") String token, @Path("email") String email);

    @GET("/cleaning-companies/{email}/cleaning-services")
    Call<ArrayList<Service>> getServiceData(@Header("Authorization") String token, @Path("email") String email);

    @GET("/customer-companies/{email}/rooms")
    Call<ArrayList<Room>> getRoomData(@Header("Authorization") String token, @Path("email") String email);

    @GET("/contracts/customer-company/{email}")
    Call<ArrayList<Contract>> getCustomerContracts(@Header("Authorization") String token, @Path("email") String email);

    @GET("/contracts/cleaning-company/{email}")
    Call<ArrayList<Contract>> getCleaningContracts(@Header("Authorization") String token, @Path("email") String email);

    @PUT("/cleaning-companies/{email}/cleaning-services")
    Call<Service> updateService(@Header("Authorization") String token, @Path("email") String email,
                                @Body Service service);

    @PUT("/cleaning-companies")
    Call<Company> updateCleaning(@Header("Authorization") String token, @Body Company company);

    @PUT("/customer-companies")
    Call<Company> updateCustomer(@Header("Authorization") String token, @Body Company company);

    @GET("/cleaning-companies/cleaning-services/{id}")
    Call<Service> getService(@Header("Authorization") String token, @Path("id") Integer id);

    @DELETE("/cleaning-companies/cleaning-services/{id}")
    Call<Service> deleteService(@Header("Authorization") String token, @Path("id") Integer id);

    @PUT("/customer-companies/{email}/rooms")
    Call<Room> updateRoom(@Header("Authorization") String token, @Path("email") String email,
                                @Body Room room);

    @GET("/customer-companies/rooms/{id}")
    Call<Room> getRoom(@Header("Authorization") String token, @Path("id") Integer id);

    @DELETE("/customer-companies/rooms/{id}")
    Call<Room> deleteRoom(@Header("Authorization") String token, @Path("id") Integer id);

    @POST("/customer-companies/{email}/rooms")
    Call<Room> addRoom(@Header("Authorization") String token, @Path("email") String email, @Body Room room);

    @POST("/cleaning-companies/{email}/cleaning-services")
    Call<Service> addService(@Header("Authorization") String token, @Path("email") String email, @Body Service service);

    @GET("/cleaning-companies")
    Call<ArrayList<Company>> getCompanies(@Header("Authorization") String token);

    @POST("/auth/register/cleaning-company")
    Call<Company> cleaningCompanySignup(@Body Company company);

    @POST("/auth/register/customer-company")
    Call<Company> customerCompanySignup(@Body Company company);

    @POST("/contracts")
    Call<Contract> signContract(@Header("Authorization") String token, @Body Contract contract);


}
