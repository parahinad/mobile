package ua.nure.cleaningservice.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contract {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("price")
    @Expose
    private Float price;
    @SerializedName("roomId")
    @Expose
    private Integer roomId;
    @SerializedName("cleaningServiceId")
    @Expose
    private Integer cleaningServiceId;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("cleaningCompanyName")
    @Expose
    private String cleaningCompanyName;
    @SerializedName("customerCompanyName")
    @Expose
    private String customerCompanyName;

    @SerializedName("serviceName")
    @Expose
    private String serviceName;

    public Contract(String date, Float price, String serviceName, Integer roomId, Integer cleaningServiceId, Integer id, String cleaningCompanyName, String customerCompanyName) {
        this.date = date;
        this.price = price;
        this.serviceName = serviceName;
        this.roomId = roomId;
        this.cleaningServiceId = cleaningServiceId;
        this.id = id;
        this.cleaningCompanyName = cleaningCompanyName;
        this.customerCompanyName = customerCompanyName;
    }

    public Contract() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Integer getCleaningServiceId() {
        return cleaningServiceId;
    }

    public void setCleaningServiceId(Integer cleaningServiceId) {
        this.cleaningServiceId = cleaningServiceId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCleaningCompanyName() {
        return cleaningCompanyName;
    }

    public void setCleaningCompanyName(String cleaningCompanyName) {
        this.cleaningCompanyName = cleaningCompanyName;
    }

    public String getCustomerCompanyName() {
        return customerCompanyName;
    }

    public void setCustomerCompanyName(String customerCompanyName) {
        this.customerCompanyName = customerCompanyName;
    }


    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }



}
