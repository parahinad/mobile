package ua.nure.cleaningservice.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Company {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("userRole")
    @Expose
    private String userRole;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("address")
    @Expose
    private Address address;

    private static Company mInstance;

    private Company() {}

    public Company(String id, String name, String email, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public static Company getInstance() {
        if(mInstance == null) {
            mInstance = new Company();
        }
        return mInstance;
    }

    public String getId() {
        return id;
    }

    public Company setId(String id) {
        this.id = id;
        return mInstance;
    }

    public String getName() {
        return name;
    }

    public Company setName(String name) {
        this.name = name;
        return mInstance;
    }

    public String getEmail() {
        return email;
    }

    public Company setEmail(String email) {
        this.email = email;
        return mInstance;
    }

    public String getPassword() {
        return password;
    }

    public Company setPassword(String password) {
        this.password = password;
        return mInstance;
    }

    public String getCompanyId() {
        return companyId;
    }

    public Company setCompanyId(String companyId) {
        this.companyId = companyId;
        return mInstance;
    }

    public String getToken() {
        return token;
    }

    public Company setToken(String token) {
        this.token = token;
        return mInstance;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Company setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return mInstance;
    }

    public String getUserRole() {
        return userRole;
    }

    public Company setUserRole(String userRole) {
        this.userRole = userRole;
        return mInstance;
    }


    public Address getAddress() {
        return address;
    }

    public Company setAddress(Address address) {
        this.address = address;
        return mInstance;
    }
}