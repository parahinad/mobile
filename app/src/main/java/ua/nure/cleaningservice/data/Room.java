package ua.nure.cleaningservice.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Room {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("roomType")
    @Expose
    private String roomType;
    @SerializedName("floor")
    @Expose
    private Integer floor;
    @SerializedName("windowsCount")
    @Expose
    private Integer windowsCount;
    @SerializedName("area")
    @Expose
    private Float area;
    @SerializedName("lastCleaning")
    @Expose
    private String lastCleaning;
    @SerializedName("smartDevice")
    @Expose
    private SmartDevice mSmartDevice;

    public Room(Integer id, String roomType, Integer floor, Integer windowsCount, Float area, String lastCleaning, SmartDevice smartDevice) {
        this.id = id;
        this.roomType = roomType;
        this.floor = floor;
        this.windowsCount = windowsCount;
        this.area = area;
        this.lastCleaning = lastCleaning;
        mSmartDevice = smartDevice;
    }

    public Room() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Integer getWindowsCount() {
        return windowsCount;
    }

    public void setWindowsCount(Integer windowsCount) {
        this.windowsCount = windowsCount;
    }

    public Float getArea() {
        return area;
    }

    public void setArea(Float area) {
        this.area = area;
    }

    public String getLastCleaning() {
        return lastCleaning;
    }

    public void setLastCleaning(String lastCleaning) {
        this.lastCleaning = lastCleaning;
    }

    public SmartDevice getSmartDevice() {
        return mSmartDevice;
    }

    public void setSmartDevice(SmartDevice smartDevice) {
        this.mSmartDevice = smartDevice;
    }

}
