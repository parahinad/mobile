package ua.nure.cleaningservice.ui.add;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.cleaningservice.R;
import ua.nure.cleaningservice.data.Company;
import ua.nure.cleaningservice.data.Service;
import ua.nure.cleaningservice.network.JSONPlaceHolderApi;
import ua.nure.cleaningservice.network.NetworkService;
import ua.nure.cleaningservice.ui.MenuActivity;
import ua.nure.cleaningservice.util.InternetConnection;
import ua.nure.cleaningservice.util.Verification;

public class AddServicesActivity extends AppCompatActivity {

    private static final String TAG = "AddServicesActivity";
    Button mCancelButton, mSaveButton;
    Service mService;
    TextView labelTV;
    EditText mNameET, mRoomTypeET, mDescET, mMinAreaET, mMaxAreaET, mPricePMET;
    private JSONPlaceHolderApi mApi;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);

        mCancelButton = findViewById(R.id.cancel_add_service_btn);
        mSaveButton = findViewById(R.id.save_add_service_btn);
        labelTV = findViewById(R.id.add_service_label);
        mNameET = findViewById(R.id.add_name_input);
        mNameET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyName(this, mNameET);
            }
        });
        mRoomTypeET = findViewById(R.id.add_type_input);
        mRoomTypeET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyType(this, mRoomTypeET);
            }
        });
        mDescET = findViewById(R.id.add_desc_input);
        mDescET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyDesc(this, mDescET);
            }
        });
        mMinAreaET = findViewById(R.id.add_min_area_input);
        mMinAreaET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyArea(this, mMinAreaET);
            }
        });
        mMaxAreaET = findViewById(R.id.add_max_area_input);
        mMaxAreaET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyArea(this, mMaxAreaET);
            }
        });
        mPricePMET = findViewById(R.id.add_price_pm_input);
        mPricePMET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyPrice(this, mPricePMET);
            }
        });
        mService = new Service();

        labelTV.setText(R.string.add_service);
        token = "Bearer " + Company.getInstance().getToken();

        mApi = NetworkService.getInstance().getApiService();

        mCancelButton.setOnClickListener(v -> {
            finish();
        });

        mSaveButton.setOnClickListener(v -> {
            addService();
        });
    }

    private void addService() {
        if (!Verification.verifyName(this, mRoomTypeET)
                || !Verification.verifyType(this, mRoomTypeET)
                || !Verification.verifyDesc(this, mDescET)
                || !Verification.verifyArea(this, mMinAreaET)
                || !Verification.verifyArea(this, mMaxAreaET)
                || !Verification.verifyPrice(this, mPricePMET)) {
            return;
        } else if (!InternetConnection.checkConnection(getApplicationContext())) {
            Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_LONG).show();
            return;
        } else {
            mService.setName(mNameET.getText().toString());
            mService.setRoomType(mRoomTypeET.getText().toString());
            mService.setDescription(mDescET.getText().toString());
            mService.setMinArea(Integer.parseInt(mMinAreaET.getText().toString()));
            mService.setMaxArea(Integer.parseInt(mMaxAreaET.getText().toString()));
            mService.setPricePerMeter(Float.parseFloat(mPricePMET.getText().toString()));

            String email = Company.getInstance().getEmail();
            mApi.addService(token, email, mService).enqueue(addServiceCallback);
            Intent intent = new Intent(AddServicesActivity.this, MenuActivity.class);
            startActivity(intent);
        }
    }

    Callback<Service> addServiceCallback = new Callback<Service>() {
        @Override
        public void onResponse(Call<Service> call, Response<Service> response) {
            if (response.isSuccessful()) {
                System.out.println(response.body());
                finish();
            }
        }

        @Override
        public void onFailure(Call<Service> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };
}
