package ua.nure.cleaningservice.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import ua.nure.cleaningservice.R;

public class MapActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        getSupportFragmentManager().beginTransaction().replace(
                R.id.fragment_container, new MapFragment()
        ).commit();
    }
}