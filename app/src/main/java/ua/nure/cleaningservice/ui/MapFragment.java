package ua.nure.cleaningservice.ui;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.cleaningservice.R;
import ua.nure.cleaningservice.data.Address;
import ua.nure.cleaningservice.data.Company;
import ua.nure.cleaningservice.network.JSONPlaceHolderApi;
import ua.nure.cleaningservice.network.NetworkService;

public class MapFragment extends Fragment {

    private static final String TAG = ".MapFragment";
    private static final int REQUEST_CODE = 1;

    private GoogleMap mMap;
    private Location mCurrentLocation;
    private SupportMapFragment mMapFragment;
    private MapMarkerIcon mMapMarkerIcon;
    JSONPlaceHolderApi mApi;
    private List<Company> mCompanies;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        mMapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);


        mMapMarkerIcon = new MapMarkerIcon();
        mCompanies = new ArrayList<>();

//        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        fetchLastLocation();


        return view;
    }

    private void fetchLastLocation() {
        mCurrentLocation = ua.nure.cleaningservice.util.Location.getLastLocation();
        if (mCurrentLocation != null) {
            mMapFragment.getMapAsync(googleMap -> {
                mMap = googleMap;
                showLocation(mCurrentLocation);
                initializeData();
            });
        } else {
            ua.nure.cleaningservice.util.Location.renewLocation(getActivity(), REQUEST_CODE, runnable);
        }
    }

    Runnable runnable = () ->
            mMapFragment.getMapAsync(googleMap -> {
            mMap = googleMap;
            initializeData();
            showLocation(ua.nure.cleaningservice.util.Location.getLastLocation());
    });

    private void showLocation (Location location) {
        LatLng latLng = new LatLng(location.getLatitude(),
                location.getLongitude());

        BitmapDescriptor icon = mMapMarkerIcon.getCurrentLocationIcon(getContext());

        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .title(getString(R.string.current_location))
                .icon(icon);
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
        mMap.addMarker(markerOptions);

    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        switch(requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLastLocation();
                }
                break;
        }
    }

    private class MapMarkerIcon {

        private BitmapDescriptor currentLocationIcon;

        public BitmapDescriptor getCurrentLocationIcon(Context context) {
            if(currentLocationIcon == null) { createCurrentLocationIcon(context); }
            return currentLocationIcon;
        }

        private void createCurrentLocationIcon(Context context) {
            Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_my_location_background);
            background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
            Drawable vectorDrawable = ContextCompat.getDrawable(context, R.drawable.ic_my_location);
            vectorDrawable.setBounds(18, 18, vectorDrawable.getIntrinsicWidth() + 18, vectorDrawable.getIntrinsicHeight() + 18);
            Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            background.draw(canvas);
            vectorDrawable.draw(canvas);
            currentLocationIcon = BitmapDescriptorFactory.fromBitmap(bitmap);
        }

        public BitmapDescriptor getRoutePointIcon(Context context) {
            @DrawableRes int resource = R.drawable.ic_route_point_blue;

            Drawable drawable = ContextCompat.getDrawable(context, resource);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawable.draw(canvas);
            return BitmapDescriptorFactory.fromBitmap(bitmap);
        }

    }

    private void initializeData() {
        String token = "Bearer " + Company.getInstance().getToken();
        mApi = NetworkService.getInstance().getApiService();
        mApi.getCompanies(token).enqueue(companiesCallback);
    }


    Callback<ArrayList<Company>> companiesCallback = new Callback<ArrayList<Company>>() {
        @Override
        public void onResponse(Call<ArrayList<Company>> call, Response<ArrayList<Company>> response) {
            if (!response.isSuccessful()) {
                System.out.println(response.code());
                return;
            }
            ArrayList<Company> companies = response.body();
            for (Company company : companies) {
                Company company1 = new Company(company.getId(), company.getName(), company.getEmail(), company.getPhoneNumber());
                company1.setAddress(new Address(company.getAddress().getCountry(), company.getAddress().getCity(), company.getAddress().getStreet(), company.getAddress().getHouseNumber(), company.getAddress().getLatitude(), company.getAddress().getLongitude()));
                mCompanies.add(company1);
            }
            printCompanies();
        }

        @Override
        public void onFailure(Call<ArrayList<Company>> call, Throwable t) {
            System.out.println(t);
            Log.i(TAG, t.getMessage());
        }
    };

    private void printCompanies() {
        List<LatLng> latLngList = new ArrayList<>();
        List<Marker> markerList = new ArrayList<>();

        for (int i = 0; i < mCompanies.size(); i++) {
            LatLng latLng = new LatLng(Double.parseDouble(mCompanies.get(i).getAddress().getLatitude()),
                    Double.parseDouble(mCompanies.get(i).getAddress().getLongitude()));
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .title(mCompanies.get(i).getName())
                    .icon(new MapMarkerIcon().getRoutePointIcon(getContext()))
                    .anchor(0.5f, 0.5f);
            Marker marker = mMap.addMarker(markerOptions);
            latLngList.add(latLng);
            markerList.add(marker);
        }
    }
}
