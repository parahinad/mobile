package ua.nure.cleaningservice.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import ua.nure.cleaningservice.R;
import ua.nure.cleaningservice.data.Company;


public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Company.getInstance().getUserRole().equals(getString(R.string.cleaning))){
            setContentView(R.layout.activity_cleaning_menu);
        }else if(Company.getInstance().getUserRole().equals(getString(R.string.customer))){
            setContentView(R.layout.activity_customer_menu);
        }
        init();
    }

    private void init() {
        if(Company.getInstance().getUserRole().equals(getString(R.string.cleaning))){
            findViewById(R.id.services_btn).setOnClickListener(v -> goTo(ServicesActivity.class));
        }else if(Company.getInstance().getUserRole().equals(getString(R.string.customer))){
            findViewById(R.id.rooms_btn).setOnClickListener(v -> goTo(RoomsActivity.class));
            findViewById(R.id.search_btn).setOnClickListener(v -> goTo(SearchActivity.class));
            findViewById(R.id.map_btn).setOnClickListener(v -> goTo(MapActivity.class));
        }
        findViewById(R.id.profile_btn).setOnClickListener(v -> goTo(ProfileActivity.class));
        findViewById(R.id.contracts_btn).setOnClickListener(v -> goTo(ContractsActivity.class));
    }

    private void goTo(Class cls) {
        Intent intent = new Intent(MenuActivity.this, cls);
        startActivity(intent);
    }
}