package ua.nure.cleaningservice.ui.edit;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.cleaningservice.R;
import ua.nure.cleaningservice.data.Company;
import ua.nure.cleaningservice.data.Room;
import ua.nure.cleaningservice.network.JSONPlaceHolderApi;
import ua.nure.cleaningservice.network.NetworkService;
import ua.nure.cleaningservice.ui.MenuActivity;

public class EditRoomActivity extends AppCompatActivity {

    private static final String TAG = "EditRoomActivity";
    Button mCancelButton, mSaveButton;
    Room mRoom;
    TextView labelTV;
    EditText mRoomTypeET, mFloorET, mWinCountET, mAreaET;
    private JSONPlaceHolderApi mApi;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_room);

        mCancelButton = findViewById(R.id.cancel_add_room_btn);
        mSaveButton = findViewById(R.id.save_add_room_btn);
        labelTV = findViewById(R.id.add_room_label);
        mRoomTypeET = findViewById(R.id.add_type_input);
        mFloorET = findViewById(R.id.add_floor_input);
        mWinCountET = findViewById(R.id.add_win_count_input);
        mAreaET = findViewById(R.id.add_area_input);

        labelTV.setText(R.string.edit_room);
        token = "Bearer " + Company.getInstance().getToken();

        mApi = NetworkService.getInstance().getApiService();

        getRoom();
        mCancelButton.setOnClickListener(v -> {
            finish();
        });

        mSaveButton.setOnClickListener(v -> {
            saveRoom();
        });
    }

    private void saveRoom() {
        mRoom.setRoomType(mRoomTypeET.getText().toString());
        mRoom.setFloor(Integer.parseInt(mFloorET.getText().toString()));
        mRoom.setWindowsCount(Integer.parseInt(mWinCountET.getText().toString()));
        mRoom.setArea(Float.parseFloat(mAreaET.getText().toString()));

        String email = Company.getInstance().getEmail();
        mApi.updateRoom(token, email, mRoom).enqueue(editRoomCallback);
        Intent intent = new Intent(EditRoomActivity.this, MenuActivity.class);
        startActivity(intent);
    }

    Callback<Room> editRoomCallback = new Callback<Room>() {
        @Override
        public void onResponse(Call<Room> call, @NotNull Response<Room> response) {
            if (response.isSuccessful()) {
                System.out.println(response.body());
                finish();
            }
        }

        @Override
        public void onFailure(Call<Room> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };

    private void getRoom() {
        mApi.getRoom(token, getIntent().getIntExtra("rId", -1)).enqueue(roomCallBack);
    }

    Callback<Room> roomCallBack = new Callback<Room>() {
        @Override
        public void onResponse(Call<Room> call, Response<Room> response) {
            if (response.isSuccessful()) {
                mRoomTypeET.setText(response.body().getRoomType());
                mAreaET.setText(String.format(Locale.getDefault(), "%f", response.body().getArea()));
                mFloorET.setText(String.format(Locale.getDefault(), "%d", response.body().getFloor()));
                mWinCountET.setText(String.format(Locale.getDefault(), "%d", response.body().getWindowsCount()));
                mRoom = new Room(response.body().getId(), response.body().getRoomType(), response.body().getFloor(), response.body().getWindowsCount(), response.body().getArea(), response.body().getLastCleaning(), response.body().getSmartDevice());
            }
        }

        @Override
        public void onFailure(Call<Room> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };
}
