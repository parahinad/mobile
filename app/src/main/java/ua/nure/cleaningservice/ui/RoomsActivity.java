package ua.nure.cleaningservice.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.cleaningservice.R;
import ua.nure.cleaningservice.data.Company;
import ua.nure.cleaningservice.data.Room;
import ua.nure.cleaningservice.network.JSONPlaceHolderApi;
import ua.nure.cleaningservice.network.NetworkService;
import ua.nure.cleaningservice.ui.add.AddRoomActivity;
import ua.nure.cleaningservice.ui.rva.RoomsRVA;

public class RoomsActivity extends AppCompatActivity {

    private static final String TAG = "ServicesActivity";

    private List<Room> mRooms;
    private RecyclerView mRecyclerView;
    private JSONPlaceHolderApi mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        mRecyclerView = (RecyclerView) findViewById(R.id.rooms_rv);
        ImageButton backButton = findViewById(R.id.back_btn);
        ImageButton addButton = findViewById(R.id.add_room_btn);

        mApi = NetworkService.getInstance().getApiService();

        LinearLayoutManager llm = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setHasFixedSize(true);

        backButton.setOnClickListener((v) -> {
            navigateToScreen(MenuActivity.class);
            finish();
        });

        addButton.setOnClickListener((v) -> {
            navigateToScreen(AddRoomActivity.class);
            finish();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeData();
    }

    private void initializeData(){
        mRooms = new ArrayList<>();
        String email = Company.getInstance().getEmail();
        String token = "Bearer " + Company.getInstance().getToken();
        mApi.getRoomData(token, email).enqueue(roomCallback);
    }


    Callback<ArrayList<Room>> roomCallback = new Callback<ArrayList<Room>>() {
        @Override
        public void onResponse(Call<ArrayList<Room>> call, Response<ArrayList<Room>> response) {
            if(!response.isSuccessful()) {
                System.out.println(response.code());
                return;
            }

            ArrayList<Room> roomList = response.body();
            for (Room room : roomList) {
                mRooms.add(new Room(room.getId(), room.getRoomType(), room.getFloor(), room.getWindowsCount(), room.getArea(), room.getLastCleaning(), room.getSmartDevice()));
            }
            initializeAdapter();
        }

        @Override
        public void onFailure(Call<ArrayList<Room>> call, Throwable t) {
            System.out.println(t);
            Log.i(TAG, t.getMessage());
        }
    };

    private void initializeAdapter(){
        RoomsRVA adapter = new RoomsRVA(this, mRooms);
        mRecyclerView.setAdapter(adapter);
    }

    private void navigateToScreen(Class cls) {
        Intent intent = new Intent(RoomsActivity.this,
                cls);
        startActivity(intent);
    }
}