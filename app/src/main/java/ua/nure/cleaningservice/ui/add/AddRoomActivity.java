package ua.nure.cleaningservice.ui.add;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.cleaningservice.R;
import ua.nure.cleaningservice.data.Company;
import ua.nure.cleaningservice.data.Room;
import ua.nure.cleaningservice.network.JSONPlaceHolderApi;
import ua.nure.cleaningservice.network.NetworkService;
import ua.nure.cleaningservice.ui.MenuActivity;
import ua.nure.cleaningservice.util.InternetConnection;
import ua.nure.cleaningservice.util.Verification;

public class AddRoomActivity extends AppCompatActivity {

    private static final String TAG = "AddRoomActivity";
    Button mCancelButton, mSaveButton;
    Room mRoom;
    TextView labelTV;
    EditText mRoomTypeET, mFloorET, mWinCountET, mAreaET;
    private JSONPlaceHolderApi mApi;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_room);

        mCancelButton = findViewById(R.id.cancel_add_room_btn);
        mSaveButton = findViewById(R.id.save_add_room_btn);
        labelTV = findViewById(R.id.add_room_label);

        mRoomTypeET = findViewById(R.id.add_type_input);
        mRoomTypeET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyType(this, mRoomTypeET);
            }
        });
        mFloorET = findViewById(R.id.add_floor_input);
        mFloorET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyFloor(this, mFloorET);
            }
        });
        mWinCountET = findViewById(R.id.add_win_count_input);
        mWinCountET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyWinCount(this, mWinCountET);
            }
        });
        mAreaET = findViewById(R.id.add_area_input);
        mAreaET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyArea(this, mAreaET);
            }
        });

        labelTV.setText(R.string.add_room);
        token = "Bearer " + Company.getInstance().getToken();

        mApi = NetworkService.getInstance().getApiService();
        mRoom = new Room();

        mCancelButton.setOnClickListener(v -> {
            finish();
        });

        mSaveButton.setOnClickListener(v -> {
            addRoom();
        });
    }

    private void addRoom() {
        if (!Verification.verifyType(this, mRoomTypeET)
                || !Verification.verifyWinCount(this, mWinCountET)
                || !Verification.verifyFloor(this, mFloorET)
                || !Verification.verifyArea(this, mAreaET)) {
            return;
        } else if (!InternetConnection.checkConnection(getApplicationContext())) {
            Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_LONG).show();
            return;
        } else {
            mRoom.setRoomType(mRoomTypeET.getText().toString());
            mRoom.setFloor(Integer.parseInt(mFloorET.getText().toString()));
            mRoom.setWindowsCount(Integer.parseInt(mWinCountET.getText().toString()));
            mRoom.setArea(Float.parseFloat(mAreaET.getText().toString()));
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
            mRoom.setLastCleaning(dateFormat.format(new Date()));

            String email = Company.getInstance().getEmail();
            mApi.addRoom(token, email, mRoom).enqueue(editRoomCallback);
            Intent intent = new Intent(AddRoomActivity.this, MenuActivity.class);
            startActivity(intent);
        }
    }

    Callback<Room> editRoomCallback = new Callback<Room>() {
        @Override
        public void onResponse(Call<Room> call, @NotNull Response<Room> response) {
            if (response.isSuccessful()) {
                System.out.println(response.body());
                finish();
            }
        }

        @Override
        public void onFailure(Call<Room> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };
}
