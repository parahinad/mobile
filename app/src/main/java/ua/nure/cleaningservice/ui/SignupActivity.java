package ua.nure.cleaningservice.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.location.FusedLocationProviderClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.cleaningservice.R;
import ua.nure.cleaningservice.data.Address;
import ua.nure.cleaningservice.data.Company;
import ua.nure.cleaningservice.network.NetworkService;
import ua.nure.cleaningservice.util.InternetConnection;
import ua.nure.cleaningservice.util.Verification;

public class SignupActivity extends AppCompatActivity {

    private static final String TAG = "SignUpActivity";
    private static final int REQUEST_CODE = 1;

    EditText mName, mEmail, mPhone, mPassword;
    Button mConfirm;
    Spinner mRole;
    LinearLayout mGoToLogin;
    ArrayList<String> roles;

    String companyName;
    int companyId;
    int[] ids;
    List<Company> company;
    Context context;

    private Location mCurrentLocation;
    private Company mCurrentCompany;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signup);

        context = this;

        init();
    }

    private void init() {
        mName = findViewById(R.id.name);
        mRole = findViewById(R.id.get_role_spinner);
        roles = new ArrayList<>();
        roles.add(getString(R.string.customer));
        roles.add(getString(R.string.cleaning));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, roles);
        mRole.setAdapter(adapter);

        mName.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyName(this, mName);
            }
        });

        mEmail = findViewById(R.id.email);
        mEmail.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyEmail(this, mEmail);
            }
        });

        mPhone = findViewById(R.id.phone);
        mPhone.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyPhone(this, mPhone);
            }
        });

        mPassword = findViewById(R.id.password);
        mPassword.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyPassword(this, mPassword);
            }
        });

        mConfirm = findViewById(R.id.signup_button);
        mConfirm.setOnClickListener(v -> signUp(
                mName.getText().toString(),
                mEmail.getText().toString(),
                mPhone.getText().toString(),
                mPassword.getText().toString(),
                mRole.getSelectedItem().toString()
        ));



        mGoToLogin = findViewById(R.id.go_to_signin);
        mGoToLogin.setOnClickListener(v -> {
            Intent intent = new Intent(SignupActivity.this, SigninActivity.class);
            startActivity(intent);
        });
    }

    //TODO sign up with location
    private void signUp(String name, String email, String phone, String password, String role) {
        if (!Verification.verifyName(this, mName)
                || !Verification.verifyEmail(this, mEmail)
                || !Verification.verifyPhone(this, mPhone)
                || !Verification.verifyPassword(this, mPassword)) {
            return;
        } else if (!InternetConnection.checkConnection(getApplicationContext())) {
            Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_LONG).show();
            return;
        } else {
            Company company = Company.getInstance()
                    .setName(name)
                    .setEmail(email)
                    .setPhoneNumber(phone)
                    .setPassword(password)
                    .setUserRole(role);
            doSignup();
        }
    }

    private void doSignup() {
        mCurrentLocation = ua.nure.cleaningservice.util.Location.getLastLocation();
        if (mCurrentLocation != null) {
            callSignup.run();
        } else {
            ua.nure.cleaningservice.util.Location.getAddressByLocation(this, REQUEST_CODE, callSignup);
        }
    }

    Runnable callSignup = () ->
    {
        Company.getInstance()
                .setAddress(Address.getInstance());

        if (Company.getInstance().getUserRole().equals(getString(R.string.customer))) {
            NetworkService.getInstance()
                    .getApiService()
                    .customerCompanySignup(Company.getInstance())
                    .enqueue(new Callback<Company>() {
                        @Override
                        public void onResponse(Call<Company> call, Response<Company> response) {
                            if(!response.isSuccessful()) {
                                Log.i(TAG, response.message() + getResources().getString(R.string.check_creds) + response.code());
                                Toast.makeText(
                                        SignupActivity.this,
                                        response.message(),
                                        Toast.LENGTH_SHORT
                                ).show();
                            } else {
                                Intent intent = new Intent(SignupActivity.this,
                                        SigninActivity.class);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onFailure(Call<Company> call, Throwable t) {
                            Log.i(TAG, t.getMessage());
                            Toast.makeText(
                                    SignupActivity.this,
                                    t.getMessage(),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    });
        } else if (Company.getInstance().getUserRole().equals(getString(R.string.cleaning))) {
            NetworkService.getInstance()
                    .getApiService()
                    .cleaningCompanySignup(Company.getInstance())
                    .enqueue(new Callback<Company>() {
                        @Override
                        public void onResponse(Call<Company> call, Response<Company> response) {
                            if(!response.isSuccessful()) {
                                Log.i(TAG, response.message() + getResources().getString(R.string.check_creds) + response.code());
                                Toast.makeText(
                                        SignupActivity.this,
                                        response.message(),
                                        Toast.LENGTH_SHORT
                                ).show();
                            } else {
                                Intent intent = new Intent(SignupActivity.this,
                                        SigninActivity.class);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onFailure(Call<Company> call, Throwable t) {
                            Log.i(TAG, t.getMessage());
                            Toast.makeText(
                                    SignupActivity.this,
                                    t.getMessage(),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    });
        }
    };

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        switch(requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    doSignup();
                }
                break;
        }
    }
}

